from datetime import datetime
from mqtt_parser import addDataHumidityList, addDataTemperatureList, getHumidity, getHumidityList, getMotion, getRFIDData, getTemperature, getTemperatureList,validateRfidTag,addRFIDMatchedList,getRFIDmatchedList,getSmokeData,checkSmokelevel,addSmokeLevelList,getSmokeList
import eventlet
import json
from flask import Flask, render_template
from flask_mqtt import Mqtt
import pandas as pd
import dash_core_components as dcc
import dash_html_components as html
from dash.exceptions import PreventUpdate
from flask_socketio import SocketIO
from flask_bootstrap import Bootstrap
import os

eventlet.monkey_patch()

app = Flask(__name__,template_folder='templates')
app.config['SECRET'] = 'my secret key'
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['MQTT_BROKER_URL'] = 'broker.hivemq.com'
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'

# Parameters for SSL enabled
# app.config['MQTT_BROKER_PORT'] = 8883
# app.config['MQTT_TLS_ENABLED'] = True
# app.config['MQTT_TLS_INSECURE'] = True
# app.config['MQTT_TLS_CA_CERTS'] = 'ca.crt'

temperature_topic = "/building1/room1/temp"
mq_topic = "/building1/room1/mq"
motion_detect_topic = "/building1/room1/pir1"
access_control_topic = "/building1/room1/rfid"

# Topics for actuators
smoke_alarm_topic = "/building1/room1/smokealarm"
light_topic = "/building1/room1/light"
door_unlock_topic = "/building1/room1/door"
hvac_topic = "/building1/room1/hvac"

# temp threshold
hvac_temp_threshold_topic = "/building1/room1/hvac/tempthreshold"

mqtt = Mqtt(app)
socketio = SocketIO(app)
bootstrap = Bootstrap(app)


@socketio.on('connected')
def handle_id(data):
    print(data)

@socketio.on('publish_smoke_alarm')
def publish_smoke_alarm(message):
    mqtt.publish(smoke_alarm_topic,message)

@socketio.on('publish_hvac')
def publish_hvac(message):
    mqtt.publish(hvac_topic,message)

@socketio.on('publish_light')
def publish_light(message):
    mqtt.publish(light_topic,message)

@socketio.on('publish_door_unlock')
def publish_door_unlock(message):
    mqtt.publish(door_unlock_topic,message)

@socketio.on('connect')
def test_connect(auth):
    print("Server Connect")

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    print("Connected to broker")
    mqtt.subscribe(temperature_topic)
    mqtt.subscribe(mq_topic)
    mqtt.subscribe(motion_detect_topic)
    mqtt.subscribe(access_control_topic)

    mqtt.subscribe(smoke_alarm_topic)
    mqtt.subscribe(light_topic)
    mqtt.subscribe(door_unlock_topic)
    mqtt.subscribe(hvac_topic)

    mqtt.subscribe(hvac_temp_threshold_topic)

@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )
    print(data)
    if(data['topic'] == temperature_topic):
        temperature = getTemperature(str(data['payload']))
        addDataTemperatureList(temperature)
        humidity = getHumidity(str(data['payload']))
        addDataHumidityList(humidity)

        data['topic'] = "Temperature"
        data['payload'] = temperature
        socketio.emit('mqtt_message', data=data)

        data['topic'] = "Humidity"
        data['payload'] = humidity
        socketio.emit('mqtt_message', data=data)

        data['topic'] = "TemperatureList"
        data_list = getTemperatureList()
        data['payload'] = data_list[0]
        data['labels'] = data_list[1]
        socketio.emit('mqtt_message', data=data)

        data['topic'] = "HumidityList"
        data_list = getHumidityList()
        data['payload'] = data_list[0]
        data['labels'] = data_list[1]
        socketio.emit('mqtt_message', data=data)
    
    elif (data['topic'] == access_control_topic):
        rfid = getRFIDData(str(data['payload']))
        addRFIDMatchedList(rfid)        
        data['topic'] = "RFID"
        data['payload'] = rfid + "-" + validateRfidTag(rfid)
        socketio.emit('mqtt_message', data=data)

        data['topic'] = "Rfidlist"
        data_list = getRFIDmatchedList()
        data['payload'] = data_list
        socketio.emit('mqtt_message', data=data)

    elif (data['topic'] == mq_topic):
        smokelevel= checkSmokelevel(getSmokeData(str(data['payload'])))
        addSmokeLevelList(smokelevel)
        data['topic'] = "Smoke"
        if(smokelevel == '1'):
            smokelevel = "Detected"
        else :
            smokelevel = "Not Detected"    
        data['payload'] = getSmokeData(smokelevel)
        socketio.emit('mqtt_message', data=data)

        data['topic'] = "SmokeList"
        data_list = getSmokeList()
        data['payload'] = data_list[0]
        data['labels'] = data_list[1]
        socketio.emit('mqtt_message', data=data)

    elif(data['topic'] == motion_detect_topic):
        data['topic'] = "MotionDetect"
        data['payload'] = getMotion(str(data['payload']))
        socketio.emit('mqtt_message', data=data)

    elif(data['topic'] == smoke_alarm_topic):
        data['topic'] = "SmokeAlarm"
        data['payload'] = (str(data['payload']))
        socketio.emit('mqtt_act_message', data=data) 

    elif(data['topic'] == light_topic):
        data['topic'] = "Light"
        data['payload'] = (str(data['payload']))
        socketio.emit('mqtt_act_message', data=data) 

    elif(data['topic'] == door_unlock_topic):
        data['topic'] = "Door"
        data['payload'] = (str(data['payload']))
        socketio.emit('mqtt_act_message', data=data) 

    elif(data['topic'] == hvac_topic):
        data['topic'] = "Hvac"
        data['payload'] = (str(data['payload']))
        socketio.emit('mqtt_act_message', data=data)

    elif(data['topic'] == hvac_temp_threshold_topic):
        set_temp_threshold(data['payload'])
'''
@mqtt.on_topic(temperature_topic)
def handle_temp_topic_data(client, userdata, message):
    payload = message.payload.decode()
    print("On topic")
    print(payload
    )
'''

def set_temp_threshold(temp):
    data = dict(
        topic = "config_thr_temp",
        payload = temp
    )
    print("Send Threshold")
    socketio.emit('config_message', data=data)

@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

@mqtt.on_log()
def handle_logging(client, userdata, level, buf):
    print(level, buf)

socketio.run(app, host='0.0.0.0', port=5000, use_reloader=True, debug=True)
