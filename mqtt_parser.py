from datetime import datetime, time
import json


temperatureList = []
temperatureValue = []
temperatureTime = []
rfidtag=['EB10390B']
SmokeList=[]
SmokeValue=[]
SmokeTime=[]
humidityList = []
humidityValue = []
humidityTime = []
rfidList = []

def getTemperature(data):
    parse_data = str(data).split(',')
    return parse_data[0]

def getTemperatureList():
   return temperatureList

def addDataTemperatureList(data):
    if len(temperatureValue) > 10:
        del temperatureValue[0]
        del temperatureTime[0]

    temperatureValue.append(data.split(',')[0])
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    temperatureTime.append(str(current_time))
    temperatureList.clear()
    temperatureList.append(temperatureValue[-10:])
    temperatureList.append(temperatureTime[-10:])

def getHumidityList():
   return humidityList

def addDataHumidityList(data):
    if len(humidityValue) > 10:
        del humidityValue[0]
        del humidityTime[0]

    humidityValue.append(str(data).split(',')[0])
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    humidityTime.append(str(current_time))
    humidityList.clear()
    humidityList.append(humidityValue[-10:])
    humidityList.append(humidityTime[-10:])

def getHumidity(data):
    parse_data = str(data).split(',')
    if(len(parse_data) > 1):
        return parse_data[1]

def getRFIDData(data):
    rfid = str(data).strip()
    return rfid 

def validateRfidTag(rfid):
    for tag in rfidtag:
        if(rfid == tag):
            return "Grant"

    else:
            return "Denied"

def addRFIDMatchedList(tag):
    now=datetime.now()
    current_time=now.strftime("%H:%M:%S")
    data = [str(tag),str(current_time),validateRfidTag(tag)]
    rfidList.append(json.dumps(data))
    
def getRFIDmatchedList():
    return rfidList

def getMotion(data):
    motion = "Not Detected"
    if(data=='1'):
        motion = "Detected"
    return motion

def getSmokeData(data):
    smoke = str(data).strip()
    return smoke

def checkSmokelevel(smoke):
        if(int(smoke) >100):
            smokelevel='1'
        else:
            smokelevel='0'
            
        return smokelevel

def addSmokeLevelList(smokelevel):
    if len(SmokeValue) > 10:
        del SmokeValue[0]
        del SmokeTime[0]
    now=datetime.now()
    current_time=now.strftime("%H:%M:%S")
    SmokeValue.append(str(smokelevel))
    SmokeTime.append(str(current_time))
    SmokeList.clear()
    SmokeList.append(SmokeValue[-10:])
    SmokeList.append(SmokeTime[-10:])


def getSmokeList():
    return SmokeList

